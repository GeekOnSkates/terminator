# Terminator.h

## Overview

Terminator is a C library for writing software that runs in a terminal.  It's designed to be cross-platform, targeting:

* Linux (tested on Ubuntu 18.04)
* Windows (tested on Windows 10)
* The Commodore 64
* The Commodore VIC-20 (partial support)

If you've worked with terminal/command-line APIs before, you probably know how vastly different they are just between Windows and Linux (never mind the awesome 8-bit stuff)!  The goal of this library is for us to be able to create terminal apps on one platform, and easily port it to many others.

# Version 1.0 documentation

```c++

// Call these at the start and end of every program.
// They do important stuff "behind the scenes": on
// some systems, they just set the default colors;
// On others, they do required startup and cleanup
// stuff required by the OS to work correctly.
void startup(void);
void done(void);

// Clears the screen
void clear(void);

// Like getchar() but it also clears the input buffer
char get_char(void);

// Gets a string of up to max characters
// (this is to avoid buffer overflows).
// Not supported on the VIC-20 (yet)!
void get_string(char *str, int max);

// Prints text center-aligned; the length has to be specified
// because otherwise, the library would need to include string.h
// which can calculate it, but would bloat apps on the 8-bit systems.
// The last parameter ("...") is whatever else you would pass to printf.
// Not supported on the VIC-20 (yet)!
void center(unsigned char length, const char *string, ...);

// The terminal's size, in characters
unsigned char ROWS, COLUMNS;```

## Future plans

The terminals I've worked with so far have a lot in common, despite their being so insanely different at the code level.  So here are a few things I would like to add at some point:

1. A way to get/set the cursor position
2. A way to show/hide the cursor
3. Get with a more experienced cc65 user somehow to finish implementing VIC-20 support, if possible
4. Early on I thought I'd add TUI (text user interface) components, and that's still something I'd like to do, but probably as a separate header (or even a separate library).

# License: GPLv3 (?)

Anyone who reads my blog knows I have no fraggin' idea how all these different licenses work.  They all claim to be "open-source" but then sneak in a bunch of hidden exceptions, encrypted deep in the Legalese with no translation to English.  I've heard the latest GPL is the best way to go, but it could be that one of the terminals I'm supporting or one of the tools I'm using may have some gotchas, and I bet you a taco those gotchas contradict ech other.  So my goal is to make it "as open as possible without opening myself up to getting sued" but if anyone besides me ever uses this, I'll be asking you legal beagles out there to help answer this question.